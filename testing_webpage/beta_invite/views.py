from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils.translation import ugettext as _
from ipware.ip import get_ip
from user_agents import parse

import common
from beta_invite import constants as cts
from beta_invite import interview_module
from beta_invite import test_module, new_user_module
from beta_invite.models import User, Visitor, Campaign, BulletType, City
from beta_invite.util import email_sender
from beta_invite.util import messenger_sender, common_senders


def get_drop_down_values(language_code):
    """
    Gets lists of drop down values for several different fields.
    Args:
        language_code: 2 digit code (eg. 'es')
    Returns: A tuple containing (Countries, Education, Professions)
    """

    countries = common.get_countries()
    cities = common.get_cities()

    professions = common.get_professions(language_code)
    work_areas = common.get_work_areas(language_code)
    education = common.get_education(language_code)
    genders = common.get_genders(language_code)
    # TODO: add new drop down here

    return countries, cities, education, professions, work_areas, genders


def translate_bullets(bullets, lang_code):
    """
    Args:
        bullets: array with bullet Objects
        lang_code: 'es' for example
    Returns: translated array
    """
    a = []
    if lang_code == 'es':

        for b in bullets:
            b.name = b.name_es
            a.append(b)
    else:
        return bullets

    return a


def translate_tests(tests, lang_code):
    """
    Args:
        tests: array with test Objects
        lang_code: 'es' for example
    Returns: translated array
    """
    a = []
    if lang_code == 'es':

        for t in tests:
            t.name = t.name_es
            a.append(t)

        return a
    else:
        return tests


def index(request):
    """
    will render a form to input user data.
    """

    # Gets information of client: such as if it is mobile
    is_desktop = not parse(request.META['HTTP_USER_AGENT']).is_mobile

    # If campaign_id is not found; will default to the default_campaign.
    campaign_id = request.GET.get('campaign_id', cts.DEFAULT_CAMPAIGN_ID)
    campaign = Campaign.objects.filter(pk=campaign_id).first()
    campaign.translate(request.LANGUAGE_CODE)

    ip = get_ip(request)

    Visitor(ip=ip, is_mobile=not is_desktop).save()

    perks = campaign.bullets.filter(bullet_type__in=BulletType.objects.filter(name='perk'))
    requirements = campaign.bullets.filter(bullet_type__in=BulletType.objects.filter(name='requirement'))

    if campaign.image is None:
        campaign.image = "default"
        campaign.save()

    param_dict = {'job_title': campaign.title,
                  'perks': translate_bullets(perks, request.LANGUAGE_CODE),
                  'requirements': translate_bullets(requirements, request.LANGUAGE_CODE),
                  'is_desktop': is_desktop,
                  'work_areas': common.get_work_areas(request.LANGUAGE_CODE),
                  'cities': common.get_cities(),
                  'default_city': common.get_city(request),
                  'image': campaign.image}

    if campaign_id is not None:
        param_dict['campaign_id'] = int(campaign_id)
        try:
            campaign = Campaign.objects.get(pk=int(campaign_id))
            campaign.translate(request.LANGUAGE_CODE)
            # if campaign exists send it.
            param_dict['campaign'] = campaign
        except ObjectDoesNotExist:
            pass

    return render(request, cts.INDEX_VIEW_PATH, param_dict)


def register(request):
    """
    Args:
        request: Request object
    Returns: Saves or updates the User, now it will not be creating new user objects for the same email.
    """
    # Gets information of client: such as if it is mobile.
    is_mobile = parse(request.META['HTTP_USER_AGENT']).is_mobile

    email = request.POST.get('email')
    name = request.POST.get('name')
    phone = request.POST.get('phone')
    work_area_id = request.POST.get('work_area_id')
    city_id = request.POST.get('city_id')

    politics_accepted = request.POST.get('politics')
    if politics_accepted:
        politics = True
    else:
        politics = False
    campaign = common.get_campaign_from_request(request)

    # Validates all fields
    if campaign and name and phone and (email or not campaign.has_email):

        country = common.get_country_with_request(request)

        user_params = {'name': name,
                       'email': email,
                       'phone': phone,
                       'work_area_id': work_area_id,
                       'country': country,
                       'city': City.objects.get(pk=city_id),
                       'ip': get_ip(request),
                       'is_mobile': is_mobile,
                       'language_code': request.LANGUAGE_CODE,
                       'politics': politics}

        user = new_user_module.user_if_exists(email, phone, campaign)
        if user:
            user = new_user_module.update_user(campaign, user, user_params, request)
        else:
            user = new_user_module.create_user(campaign, user_params, request, is_mobile)

        return redirect('/servicio_de_empleo/pruebas?campaign_id={campaign_id}&user_id={user_id}'.format(campaign_id=campaign.id,
                                                                                                         user_id=user.id))
    else:
        return HttpResponseBadRequest('<h1>HTTP CODE 400: Client sent bad request with missing params</h1>')


def tests(request):

    """
    Receives either GET or POST user and campaign ids.
    :param request: HTTP
    :return: renders tests.
    """

    campaign = common.get_campaign_from_request(request)
    tests = translate_tests(campaign.tests.all(), request.LANGUAGE_CODE)

    end_point_params = {'campaign_id': campaign.id,
                        'tests': tests,
                        }

    # Adds the user id to the params, to be able to track answers, later on.
    user = common.get_user_from_request(request)
    candidate = common.get_candidate(user, campaign)
    if not candidate:
        return redirect('/servicio_de_empleo?campaign_id={}'.format(campaign.id))
    if user is not None:
        end_point_params['user_id'] = int(user.id)

    if tests:
        return render(request, cts.TESTS_VIEW_PATH, end_point_params)
    else:
        return redirect('/servicio_de_empleo/additional_info?candidate_id={candidate_id}'.format(candidate_id=candidate.pk))


@login_required
def home(request):
    return render(request, 'success.html')


def send_interview_mail(email_template, candidate):
    """
    Args:
        email_template: name of email body, in beta_invite/util.
        candidate: Object.
    Returns: sends email.
    """
    if candidate and interview_module.has_recorded_interview(candidate.campaign):
        candidate.campaign.translate(candidate.user.language_code)

        email_sender.send(objects=candidate,
                          language_code=candidate.user.language_code,
                          body_input=email_template,
                          subject=_('You can record the interview for {campaign}').format(campaign=candidate.campaign.title))


def get_test_result(request):
    """
    Args:
        request: HTTP object
    Returns: Either end process or invites to interview.
    """
    campaign = common.get_campaign_from_request(request)
    questions_dict = test_module.get_tests_questions_dict(campaign.tests.all())
    user = common.get_user_from_request(request)
    user_id = user.id if user else None
    if not user:
        return redirect('/servicio_de_empleo?campaign_id={campaign_id}'.format(campaign_id=campaign.id))
    candidate = common.get_candidate(user, campaign)
    name = common_senders.get_first_name(candidate.user.name)

    scores = test_module.get_scores(campaign, user_id, questions_dict, request)
    evaluation = test_module.get_evaluation(scores, candidate)
    failed_scores = [s for s in evaluation.scores.all() if not s.passed and s.test.feedback_url]

    if evaluation.passed or len(failed_scores) == 0:
        return redirect('/servicio_de_empleo/additional_info?candidate_id={candidate_id}'.format(candidate_id=candidate.pk))

    return render(request, cts.FAILED_TEST_VIEW_PATH, {'candidate': candidate,
                                                       'campaign': campaign,
                                                       'candidate_id': candidate.pk,
                                                       'name': name,
                                                       'failed_scores_with_feedback': failed_scores})


def additional_info(request):
    candidate = common.get_candidate_from_request(request)
    param_dict = dict()
    countries, cities, education, professions, work_areas, genders = get_drop_down_values(request.LANGUAGE_CODE)

    # Dictionary parameters
    param_dict['candidate'] = candidate
    param_dict['genders'] = genders
    param_dict['work_areas'] = work_areas
    param_dict['education'] = education
    param_dict['professions'] = professions
    param_dict['countries'] = countries
    param_dict['cities'] = cities

    return render(request, cts.ADDITIONAL_INFO_VIEW_PATH, param_dict)


def save_partial_additional_info(request):

    if request.method == 'POST':
        candidate = common.get_candidate_from_request(request)
        new_user_module.update_user_with_request(request, candidate.user)

        return HttpResponse('')
    else:
        return HttpResponseBadRequest('<h1>HTTP CODE 400: Client sent bad request with missing params</h1>')


def active_campaigns(request):

    candidate = common.get_candidate_from_request(request)

    if candidate is not None:
        new_user_module.update_user(candidate.campaign, candidate.user, {}, request)

        last_evaluation = candidate.get_last_evaluation()
        if last_evaluation:
            test_module.update_scores(last_evaluation, last_evaluation.scores.all())

            # Uses ML to send to STC state!!! 100% automation reached?
            test_module.classify_evaluation_and_change_state(candidate,
                                                             use_machine_learning=True,
                                                             success_state='STC',
                                                             fail_state='WFI')

    return render(request, cts.ACTIVE_CAMPAIGNS_VIEW_PATH)


def add_cv(request):
    """
    When a user registers on a phone, there is no CV field. So the user is left with no CV on his/her profile.
    An email is sent to the user requesting to complete his/her profile by adding the missing CV. This rendering
    displays the missing CV interface.
    Passes around the user_id param. Transitions from GET to POST
    Args:
        request: HTTP
    Returns: Renders simple UI to add a missing CV
    """
    user_id = request.GET.get('user_id')
    return render(request, cts.ADD_CV, {'user_id': user_id})


def add_cv_changes(request):
    """
    Args:
        request: HTTP
    Returns: Adds a CV to the User profile, given that the user exists.
    """

    user_id = request.POST.get('user_id')

    if user_id:
        user = User.objects.get(pk=int(user_id))
        new_user_module.update_resource(request, user, 'curriculum_url', 'resumes')

        return render(request, cts.ACTIVE_CAMPAIGNS_VIEW_PATH, {})

    # if any inconsistency, then do nothing, ignore it.
    return render(request, cts.ADD_CV, {'user_id': user_id})


def security_politics(request):
    return render(request, cts.SECURITY_POLITICS_VIEW_PATH)
